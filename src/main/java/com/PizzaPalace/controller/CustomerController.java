package com.PizzaPalace.controller;

import com.PizzaPalace.dtos.CustomerDto;
import com.PizzaPalace.entity.Customer;
import com.PizzaPalace.helper.MessageResponse;
import com.PizzaPalace.repositories.CustomerRepository;
import com.PizzaPalace.services.CustomerService;
import jakarta.validation.Valid;
import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customers")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private CustomerRepository customerRepository;

    private Logger logger = LoggerFactory.getLogger(CustomerController.class);

    //Create Customer
    @PostMapping
    public ResponseEntity<MessageResponse> createCustomer(@Valid @RequestBody CustomerDto customerDto) {
        logger.info("Inside createCustomer Handler of CustomerController");
        boolean exist = customerRepository.existsByEmail(customerDto.getEmail());
        if (exist) {
            MessageResponse response = MessageResponse.builder().message("Customer Already Exist ..!").success(false).httpStatus(HttpStatus.INTERNAL_SERVER_ERROR).data(customerRepository.findByEmail(customerDto.getEmail())).build();
            logger.error("User Already Exist");
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

        }
        CustomerDto customer = customerService.createCustomer(customerDto);
        MessageResponse response = MessageResponse.builder().message("User Created Successfully").success(true).httpStatus(HttpStatus.OK).data(customer).build();
        logger.info("User Created Successfully");
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    //Update Customer

    @PutMapping("/{custId}")
    public ResponseEntity<MessageResponse> updateCustomer(@PathVariable Long custId, @Valid @RequestBody CustomerDto customerDto) {
        CustomerDto updateCustomer = customerService.updateCustomer(customerDto, custId);
        MessageResponse response = MessageResponse.builder().message("Customer Saved Successfully ..!!").success(true).httpStatus(HttpStatus.OK).data(updateCustomer).build();
        logger.info("Customer Added Successfully");
        return new ResponseEntity<>(response, HttpStatus.OK);

    }


    @GetMapping
    public ResponseEntity<MessageResponse> getAllCustomer() {
        logger.info("Inside GetAll Customer of Customer Controller");
        MessageResponse response = MessageResponse.builder().message("Customer Saved Successfully ..!!").success(true).httpStatus(HttpStatus.OK).data(customerService.getAllCustomer()).build();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    // Delete Customer
    @DeleteMapping("/{custId}")
    public ResponseEntity<MessageResponse> deleteCustomer(@PathVariable Long custId) {
        logger.info("Inside eleteCustomer Handler of CustomerController");
        customerService.deleteCustomer(custId);
        MessageResponse response = MessageResponse.builder()
                .message("User Deleted SuccessFully")
                .success(true)
                .httpStatus(HttpStatus.OK).data(customerRepository.findById(custId)).build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    //Search By First Name
    @GetMapping("/search/{keywords}")
    public ResponseEntity<MessageResponse> searchCustomerByFName(@PathVariable String keywords) {
        logger.info("Inside searchCustomer Handler of CustomerController");
        MessageResponse response = MessageResponse.builder().message("Customer Fetched..!!").success(true).httpStatus(HttpStatus.OK).data(customerService.searchCustomerByFirstName(keywords)).build();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    // Get By Id

    @GetMapping("/{custId}")
    public ResponseEntity<MessageResponse> getCustomerById(@PathVariable Long custId) {
        logger.info("Inside getCustomerByID Handler of CustomerController");
        MessageResponse response = MessageResponse.builder().message("Customer Fetched..!!").success(true).httpStatus(HttpStatus.OK).data(customerService.getCustomerById(custId)).build();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/email/{email}")
    public ResponseEntity<Customer> getByEmail(@PathVariable String email) {
        return new ResponseEntity<>(customerService.getCustomerByEmail(email), HttpStatus.OK);
    }

}


