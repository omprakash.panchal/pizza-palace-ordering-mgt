package com.PizzaPalace.controller;

import com.PizzaPalace.dtos.PizzasDto;
import com.PizzaPalace.entity.Pizza;
import com.PizzaPalace.helper.MessageResponse;
import com.PizzaPalace.services.PizzasServices;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pizza")
public class PizzasController {

    @Autowired
    private PizzasServices pizzasServices;
    private Logger logger = LoggerFactory.getLogger(CustomerController.class);

    // Create
    @PostMapping
    public ResponseEntity<MessageResponse> createPizza(@Valid @RequestBody PizzasDto pizzasDto) {
        logger.info("inside Create Pizza");
        PizzasDto pizza = pizzasServices.createPizza(pizzasDto);
        MessageResponse response = MessageResponse.builder().message("Pizza Created Successfully..!").success(true).httpStatus(HttpStatus.CREATED).data(pizza).build();

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    //Update Pizza

    @PutMapping("/{pizzaId}")
    public ResponseEntity<MessageResponse> updatePizza(@PathVariable Long pizzaId, @Valid @RequestBody PizzasDto pizzasDto) {
        logger.info("inside Update Pizza");
        PizzasDto updatedPizza = pizzasServices.updatePizzas(pizzasDto, pizzaId);
        MessageResponse response = MessageResponse.builder().message("Pizza updated Successfully...!").success(true).httpStatus(HttpStatus.OK).data(updatedPizza).build();

        return new ResponseEntity<>(response, HttpStatus.OK);

    }

    //Get All Pizzas
    @GetMapping
    public ResponseEntity<MessageResponse> getAllPizzas() {
        logger.info("inside GEtAll Pizza");
        MessageResponse response = MessageResponse.builder().message("All pizzas..!").success(true).httpStatus(HttpStatus.OK).data(pizzasServices.getAllPizzas()).build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    // Delete Pizza
    @DeleteMapping("/{pizzaId}")
    public ResponseEntity<MessageResponse> deletePizza(@PathVariable Long pizzaId) {
        logger.info("inside Delete Pizza");
        pizzasServices.deletePizza(pizzaId);
        MessageResponse response = MessageResponse.builder()
                .message("Pizza Deleted SuccessFully")
                .success(true)
                .httpStatus(HttpStatus.OK).data(pizzaId).build();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    //Search By Pizza Name
    @GetMapping("/search/{keywords}")
    public ResponseEntity<MessageResponse> searchPizza(@PathVariable String keywords) {
        logger.info("inside Search Pizza");
        MessageResponse response = MessageResponse.builder().message("Your Result..!").success(false).httpStatus(HttpStatus.OK).data(pizzasServices.searchByPizzaNameContaining(keywords)).build();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }


}
