package com.PizzaPalace.services;

import com.PizzaPalace.dtos.PizzasDto;
import com.PizzaPalace.entity.Pizza;
import com.PizzaPalace.helper.MessageResponse;

import java.util.List;

public interface PizzasServices {


    //create
    PizzasDto createPizza(PizzasDto pizzas);


    //update
    PizzasDto updatePizzas(PizzasDto pizzasDto, Long pizzaId);

    //delete
    MessageResponse deletePizza(Long pizzaId);


    //get all Customer
    List<Pizza> getAllPizzas();


    //search user
    List<Pizza> searchByPizzaNameContaining(String keyword);


}
