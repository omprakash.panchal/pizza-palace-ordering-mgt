package com.PizzaPalace.services;

import com.PizzaPalace.dtos.CustomerDto;
import com.PizzaPalace.entity.Customer;

import java.util.List;

public interface CustomerService {

    //create
    CustomerDto createCustomer(CustomerDto customerDto);


    //update
    CustomerDto updateCustomer(CustomerDto userDto, Long custId);

    //delete
    void deleteCustomer(Long custId);


    //get all Customer
    List<Customer> getAllCustomer();


    //get single user by id
    Customer getCustomerById(Long custId);

//    // Check Customer Exist or nit by Email
//
//    Customer isUserExist(String email);

    //get  single user by email
    Customer getCustomerByEmail(String email);

    //search user
    List<Customer> searchCustomerByFirstName(String keyword);


}
