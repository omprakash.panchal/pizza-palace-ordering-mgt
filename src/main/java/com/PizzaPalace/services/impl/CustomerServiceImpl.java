package com.PizzaPalace.services.impl;

import com.PizzaPalace.dtos.CustomerDto;
import com.PizzaPalace.entity.Customer;
import com.PizzaPalace.exceptions.InternalServerErrorException;
import com.PizzaPalace.exceptions.ResourceNotFoundException;
import com.PizzaPalace.repositories.CustomerRepository;
import com.PizzaPalace.services.CustomerService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private ModelMapper mapper;

    @Autowired
    private CustomerRepository customerRepository;

    // Create
    @Override
    public CustomerDto createCustomer(CustomerDto customerDto) {
        Customer customer = mapper.map(customerDto, Customer.class);

        Customer savedCustomer = customerRepository.save(customer);
        return mapper.map(savedCustomer, CustomerDto.class);
    }

    //Update
    @Override
    public CustomerDto updateCustomer(CustomerDto customerDto, Long custId) {

        Customer customer = customerRepository.findById(custId).orElseThrow(() -> new ResourceNotFoundException("Customer Not Found With Given Customer Id"));
        customer.setFirstName(customerDto.getFirstName());
        customer.setMiddleName(customerDto.getMiddleName());
        customer.setLastName(customerDto.getLastName());
        customer.setPassword(customerDto.getPassword());
        customer.setAddress(customerDto.getAddress());
        customer.setContactNumber(customerDto.getContactNumber());

        //Save Data
        Customer savedCustomer = customerRepository.save(customer);
        return mapper.map(savedCustomer, CustomerDto.class);

    }

    //Delete
    @Override
    public void deleteCustomer(Long custId) {
        Customer customer = customerRepository.findById(custId).orElseThrow(()
                -> new ResourceNotFoundException("Customer Not Found From This ID"));
        customerRepository.delete(customer);

    }

    //Get All Customers
    @Override
    public List<Customer> getAllCustomer() {

        return customerRepository.findAll();


    }

    //Get By Id
    @Override
    public Customer getCustomerById(Long custId) {
        Customer customer = customerRepository.findById(custId).orElseThrow(() -> new ResourceNotFoundException("User Not Found by given ID"));
        return customer;
    }


    //Get By Email
    @Override
    public Customer getCustomerByEmail(String email) {
        Customer byEmail = customerRepository.findByEmail(email);
        return byEmail;
    }

    //Search Customer
    @Override
    public List<Customer> searchCustomerByFirstName(String keyword) {

        List<Customer> byFirstNameContaining = customerRepository.findByFirstNameContaining(keyword);
        List<Customer> collect = byFirstNameContaining.stream().map(customer -> customer).collect(Collectors.toList());

        return collect;
    }
}
