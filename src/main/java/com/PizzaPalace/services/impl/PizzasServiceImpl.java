package com.PizzaPalace.services.impl;

import com.PizzaPalace.dtos.PizzasDto;
import com.PizzaPalace.entity.Pizza;
import com.PizzaPalace.exceptions.ResourceNotFoundException;
import com.PizzaPalace.helper.MessageResponse;
import com.PizzaPalace.repositories.PizzasRepository;
import com.PizzaPalace.services.PizzasServices;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PizzasServiceImpl implements PizzasServices {

    @Autowired
    private ModelMapper mapper;

    @Autowired
    private PizzasRepository pizzasRepository;

    //Create
    @Override
    public PizzasDto createPizza(PizzasDto pizzasDto) {

        Pizza pizza = mapper.map(pizzasDto, Pizza.class);
        Pizza savedPizza = pizzasRepository.save(pizza);
        PizzasDto pizzasDto1 = mapper.map(pizza, PizzasDto.class);
        return pizzasDto1;
    }


    //Update
    @Override
    public PizzasDto updatePizzas(PizzasDto pizzasDto, Long pizzaId) {

        Pizza pizza = pizzasRepository.findById(pizzaId).orElseThrow(() -> new ResourceNotFoundException("Pizza Not Found From This Id"));
        pizza.setPizzaName(pizzasDto.getPizzaName());
        pizza.setPizzaType(pizzasDto.getPizzaType());
        pizza.setDescription(pizzasDto.getDescription());
        pizza.setPriceRegSize(pizza.getPriceRegSize());
        pizza.setPriceMSize(pizza.getPriceMSize());
        pizza.setPriceLSize(pizzasDto.getPriceLSize());
        Pizza updatedPizza = pizzasRepository.save(pizza);
        return mapper.map(updatedPizza, PizzasDto.class);
    }

    //Delete
    @Override
    public MessageResponse deletePizza(Long pizzaId) {

        Pizza pizzas = pizzasRepository.findById(pizzaId).orElseThrow(() -> new ResourceNotFoundException("Pizza Not Found From This Id"));
        pizzasRepository.delete(pizzas);
        return MessageResponse.builder().message("Pizza Deleted Successfully").success(true).httpStatus(HttpStatus.NO_CONTENT).data(pizzas).build();
    }

    //Get All Pizzas
    @Override
    public List<Pizza> getAllPizzas() {
        return pizzasRepository.findAll();
    }

    // Search Pizza
    @Override
    public List<Pizza> searchByPizzaNameContaining(String keyword) {
        List<Pizza> byPizzaNameContaining = pizzasRepository.findByPizzaNameContaining(keyword);
        return byPizzaNameContaining;
    }


}
