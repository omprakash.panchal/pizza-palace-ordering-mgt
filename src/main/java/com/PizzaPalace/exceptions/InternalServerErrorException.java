package com.PizzaPalace.exceptions;

import org.springframework.validation.Errors;

public class InternalServerErrorException extends RuntimeException {
    public InternalServerErrorException() {
        super("Internal Server Error Exception Occured");
    }

    public InternalServerErrorException(String message) {
        super(message);
    }


}
