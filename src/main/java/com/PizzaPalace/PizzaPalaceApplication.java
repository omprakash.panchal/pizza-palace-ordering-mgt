package com.PizzaPalace;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PizzaPalaceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PizzaPalaceApplication.class, args);
	}

}
