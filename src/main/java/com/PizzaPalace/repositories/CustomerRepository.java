package com.PizzaPalace.repositories;

import com.PizzaPalace.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CustomerRepository extends JpaRepository<Customer,Long> {
    List<Customer> findByFirstNameContaining(String keywords);
   // List<Customer> findByFirstNameAndLastNameContaining(String keywords);
    Customer findByEmail(String email);
    boolean existsByEmail(String email);
}
