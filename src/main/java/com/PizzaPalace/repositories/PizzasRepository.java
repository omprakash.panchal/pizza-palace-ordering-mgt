package com.PizzaPalace.repositories;

import com.PizzaPalace.entity.Pizza;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PizzasRepository extends JpaRepository<Pizza,Long> {

    List<Pizza> findByPizzaNameContaining(String keywords);

}
