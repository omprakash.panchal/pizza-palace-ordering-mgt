package com.PizzaPalace.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Entity
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long custId;

    @Column(length = 30 )
    private String firstName;

    @Column(length = 30)
    private String middleName;

    @Column(length = 30)
    private String lastName;


    @Column(name = "user_email", unique = true )
    private String email;

    @Column(length = 500)
    private String password;


    @Column(length = 20)
    private String contactNumber;

    @Column(length = 200)
    private String address;

    @OneToMany(mappedBy = "customer", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Orders> orders = new ArrayList<>();
}
