package com.PizzaPalace.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Orders {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long orderId;

    private String Status;

    private Date orderTime = new Date();


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "customer_Id")
    private Customer customer;

    @OneToMany(mappedBy = "orders")
    @JoinColumn(name = "order_line_id")
    private List<OrderLine> orderLine;


}
