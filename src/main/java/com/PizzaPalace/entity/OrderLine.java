package com.PizzaPalace.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class OrderLine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long lineId;



    @NotBlank( message = "Enter Quantity ..!!")
    private Integer quantity;

    private double totalLineAmount;


    @ManyToOne(fetch = FetchType.EAGER)
    private Orders orders;

    @ManyToOne
    @JoinColumn(name = "pizza_id")
    private Pizza pizza ;

}
