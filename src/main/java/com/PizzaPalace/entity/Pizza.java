package com.PizzaPalace.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Pizza {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long pizzaId;

    @Column( length = 50, unique = true)
    private String pizzaName;

    @Column(length = 250)
    private String description;

    @Column(length = 30)
    private String pizzaType;

    @Column(length = 250)
    private String imageUrl;

    @Column(length = 25)
    private Double priceRegSize;

    @Column(length = 25)
    private Double priceMSize;

    @Column(length = 25)
    private Double priceLSize;

    @OneToMany
    private List<OrderLine> orderLine;

}
