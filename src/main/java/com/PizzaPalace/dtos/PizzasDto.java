package com.PizzaPalace.dtos;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PizzasDto {


    private Long pizzaId;

    @Size(min = 3, message = "Invalid Pizza Name !!")
    private String pizzaName;

    @Size(min = 3,max = 100 ,message = "Description is Required!!")
    private String description;

    @Size(min = 2,max = 50 ,message = "PizzaType is Required!!")
    private String pizzaType;


    private String imageUrl;


    private Double priceRegSize;


    private Double priceMSize;


    private Double priceLSize;
}
