package com.PizzaPalace.dtos;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderLineDto {

    private Long lineId;

    private Long pizzaId;

    private Integer quantity;

    private double totalLineAmount;
}
