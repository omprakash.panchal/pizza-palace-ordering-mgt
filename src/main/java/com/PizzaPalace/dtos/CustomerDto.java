package com.PizzaPalace.dtos;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CustomerDto {


    private Long custId;

    @Size(min = 3, max = 20, message = "Invalid Fisrt Name !!")
    private String firstName;

    @Size(min = 3, max = 20, message = "Invalid Middle Name !!")
    private String middleName;

    @Size(min = 3, max = 20, message = "Invalid Last Name !!")
    private String lastName;


    @NotBlank( message = "Email is required !!")
    private String email;

    @NotBlank(message = "Password is required !!")
    private String password;

    @NotBlank(message = "Contact is required !!")
    private String contactNumber;

    @NotBlank(message = "Address is required !!")
    private String address;
}
