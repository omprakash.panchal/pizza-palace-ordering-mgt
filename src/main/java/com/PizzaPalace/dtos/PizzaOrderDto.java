package com.PizzaPalace.dtos;

public class PizzaOrderDto {

    private Long pizzaId;

    private String size;

    private Integer quantity;
}
