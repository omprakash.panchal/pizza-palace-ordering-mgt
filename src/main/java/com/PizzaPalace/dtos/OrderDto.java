package com.PizzaPalace.dtos;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotBlank;
import lombok.*;

import java.util.Date;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderDto {


    private Long orderId;

    @NotBlank( message = "Please Select Customer!!")
    private long custId;

    private Long lineId;

    @NotBlank( message = "Enter Status !!")
    private String Status;

    private Date orderTime;
}
