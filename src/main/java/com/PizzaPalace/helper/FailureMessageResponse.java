package com.PizzaPalace.helper;

import lombok.*;
import org.springframework.http.HttpStatus;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FailureMessageResponse {


    private String message;

    private boolean success;

    private ErrorMesage error;
}
