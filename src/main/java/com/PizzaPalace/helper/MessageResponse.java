package com.PizzaPalace.helper;

import lombok.*;
import org.springframework.http.HttpStatus;

import java.security.PrivateKey;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MessageResponse {


    private String message;

    private boolean success;

    private HttpStatus httpStatus;
    
    private Object data;

}
