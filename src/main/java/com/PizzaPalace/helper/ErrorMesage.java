package com.PizzaPalace.helper;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ErrorMesage {
    private  int code;
    String message;

}
